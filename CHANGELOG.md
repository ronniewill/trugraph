# 3.0.0 (2022-06-25) #
## Release Highlights ##
In this release we've removed the [math-expressions](https://www.npmjs.com/package/math-expressions) library and replaced it with the [mathjs](https://www.npmjs.com/package/mathjs) library.