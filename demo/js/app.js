import TruGraph from "../../src/js"

const appContainer = document.querySelector('#app')

const demo1 = document.createElement('DIV')
demo1.id = 'demo1'

appContainer.appendChild(demo1)

const gDemo1 = new TruGraph('demo1', {
  expression: 'x^2',
  colour: '#FF0000',
  thickness: 2,
  style: 'solid',
  domain: ['negative infinity', 'infinity'],
  range: ['negative infinity', 'infinity'],
  endpoints: true,
  endpointLeft: 'arrow',
  endpointRight: 'arrow'
})

const demo2 = document.createElement('DIV')
demo2.id = 'demo2'
demo2.style = 'min-width:200px; height:300px;'

appContainer.appendChild(demo2)

const gDemo2 = new TruGraph('demo2', {
  expression: 'sin(x)',
  colour: '#FF0000',
  thickness: 2,
  style: 'solid',
  domain: ['negative infinity', 'infinity'],
  range: ['negative infinity', 'infinity'],
  endpoints: true,
  endpointLeft: 'arrow',
  endpointRight: 'arrow'
},
{
  visibleDomain: [-6.28, 6.28],
  visibleRange: [-2, 2],
  dependentVar: 'y',
  independentVar: 'x',
  border: true,
  borderColour: '#CCCCCC',
  borderWidth: 5,
  square: true,
  graphTitle: 'Sine Graph',
  x: {
    gridLines: true,
    gridStart: 0,
    gridSpace: 1.57,
    tickStart: 0,
    tickSpace: 0.753
  },
  y: {
    gridLines: true,
    gridStart: 0,
    gridSpace: 0.5,
    tickStart: 0,
    tickSpace: 0.25
  }
})