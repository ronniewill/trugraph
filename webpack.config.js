const path = require('path')

let __root = path.join(__dirname, '')
let __src = path.join(__dirname, 'src')

module.exports = {
  context: __src,
  mode: 'production',
  entry: {
    index: './js/index.js'
  },
  output: {
    filename: './js/[name].js',
    chunkFilename: './js/[name].chunk.js',
    library: 'trugraph',
    libraryTarget: 'umd',
    clean: true
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  }
};