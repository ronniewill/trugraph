import resolve from 'rollup-plugin-node-resolve'
import { TruGraph } from './src/js/index.js';
const dependencies = Object.keys(require('./package.json').dependencies)

module.exports = {
  input: 'src/js/index.js',
  output: [
    {
      file: 'dist/js/trugraph.cjs.js',
      format: 'cjs'
    },
    {
      file: 'dist/js/trugraph.amd.js',
      format: 'amd'
    },
    {
      file: 'dist/js/trugraph.umd.js',
      name: 'trugraph',
      format: 'umd'
    },
    {
      file: 'dist/js/trugraph.es.js',
      format: 'es'
    },
    {
      file: 'dist/js/trugraph.iife.js',
      format: 'iife'
    }
  ],
  plugins: [ resolve() ],
  external: dependencies
};