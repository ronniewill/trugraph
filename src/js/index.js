import * as d3 from 'd3'
import debug from 'debug'
import { defaultOptions } from './partials/defaultOptions.js'
import { defaultLayout } from './partials/defaultLayout.js'
import { create, createTrace, buildTrace } from './partials/create.js'
import { checkLayout, checkObject } from './partials/utilities.js'

const log = debug('TruGraph:log')
const error = debug('TruGraph:error')

export default function TruGraph(containerID, data, layout, options) {
  try {
    // if (options === null) {
    //   options = defaultOptions
    //   this.options = options
    // } else {
    //   checkObject(options, defaultOptions).then((response) => {
    //     options = response
    //     this.options = options
    //   })
    // }
    // if (this.options.debug === false) {
    //   log.disable()
    //   error.disable()
    // }
    log(`TruGraph is creating the graph for container id: ${containerID}`)
    if (!containerID || typeof containerID !== 'string') error ('User must provide a container id, for truGraph to apply to.')
    let updateContainerID
    if (containerID.slice(0,1) !== '#') updateContainerID = `#${containerID}`
    this.getContainerID = updateContainerID
    this.containerID = containerID
    if (Array.isArray(data) && data.length > 0) {
      this.startData = data
    } else if (!Array.isArray(data) && typeof data === 'object') {
      this.startData = [data]
    } else {
      this.startData = []
    }
    this.newData = this.startData
    if (!layout) {
      this.startLayout = defaultLayout
      this.newLayout = this.startLayout
    } else if (typeof layout !== 'object') {
      this.startLayout = defaultLayout
      this.newLayout = this.startLayout
    } else {
      this.startLayout = layout
      this.newLayout = this.startLayout
    }
    create(updateContainerID, containerID, this.newLayout)
      .then((response) => {
        if (typeof response !== 'undefined') {
          this.xScale = response[0]
          this.yScale = response[1]
          this.newLayout = response[2]
          this.bufferX = response[3]
          this.bufferY = response[4]
          let myTraces = []
          for (let i = 0; i < this.newData.length; i++) {
            myTraces.push(createTrace(this.newData[i], this.newLayout))
          }
          Promise.all(myTraces).then((responses) => {
            log('responses: ',responses)
            for (let j = 0; j < responses.length; j++) {
              buildTrace(this.newData[j], this.newLayout, responses[j], j, containerID, this.xScale, this.yScale, this.bufferX, this.bufferY)
            }
          })
        }
      })
    this.updateLayout = async (newerLayout) => {
      let checkedLayout = await checkLayout(newerLayout, this.newLayout)
      this.newLayout = checkedLayout
      document.querySelector(this.getContainerID).innerHTML = ""
      create(this.getContainerID, this.containerID, this.newLayout)
        .then((response) => {
          this.xScale = response[0]
          this.yScale = response[1]
          this.newLayout = response[2]
          this.bufferX = response[3]
          this.bufferY = response[4]
          let myTraces = []
          for (let i = 0; i < this.newData.length; i++) {
            myTraces.push(createTrace(this.newData[i], this.newLayout))
          }
          Promise.all(myTraces).then((responses) => {
            for (let j = 0; j < responses.length; j++) {
              buildTrace(this.newData[j], this.newLayout, responses[j], j, containerID, this.xScale, this.yScale, this.bufferX, this.bufferY)
            }
          })
        })
    }
    this.redraw = () => {
      document.querySelector(this.getContainerID).innerHTML = ""
      create(this.getContainerID, this.containerID, this.newLayout)
        .then((response) => {
          this.xScale = response[0]
          this.yScale = response[1]
          this.newLayout = response[2]
          this.bufferX = response[3]
          this.bufferY = response[4]
          let myTraces = []
          for (let i = 0; i < this.newData.length; i++) {
            myTraces.push(createTrace(this.newData[i], this.newLayout))
          }
          Promise.all(myTraces).then((responses) => {
            for (let j = 0; j < responses.length; j++) {
              buildTrace(this.newData[j], this.newLayout, responses[j], j, containerID, this.xScale, this.yScale, this.bufferX, this.bufferY)
            }
          })
        })
    }
  } catch (err) {
    error('TruGraph failed unexpectedly.', err)
  }
}