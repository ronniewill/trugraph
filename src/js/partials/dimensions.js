import debug from 'debug'

const log = debug('TruGraph:dimensions:log:')
const error = debug('TruGraph:dimensions:error:')

export async  function squareDims (width, height) {
  try {
    let dims = {}
    if (width >= height) {
      dims.width = height
      dims.height = height
      dims.xDiff = width - height
      dims.yDiff = 0
    } else {
      dims.width = width
      dims.height = width
      dims.xDiff = 0
      dims.yDiff = height - width
    }
    return dims
  } catch(err) {
    error('squareDims failed unexpectedly.', err)
  }
}