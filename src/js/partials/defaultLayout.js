export let defaultLayout = {
  visibleDomain: [-10, 10],
  visibleRange: [-10, 10],
  dependentVar: 'y',
  independentVar: 'x',
  border: true,
  borderColour: '#000000',
  borderWidth: 2,
  square: false,
  x: {
    gridLines: true,
    gridStart: 0,
    gridSpace: 0.5,
    tickStart: 0,
    tickSpace: 0.5
  },
  y: {
    gridLines: true,
    gridStart: 0,
    gridSpace: 0.5,
    tickStart: 0,
    tickSpace: 0.5
  },
  graphTitle: ''
}