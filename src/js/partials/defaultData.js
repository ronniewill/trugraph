export let defaultData = {
  expression: 'x^2',
  colour: '#FF0000',
  thickness: 2,
  style: 'solid',
  domain: ['negative infinity', 'infinity'],
  range: ['negative infinity', 'infinity'],
  endpoints: true,
  endpointLeft: 'arrow', // open-circle, closed-circle
  endpointLeft: 'arrow' // open-circle, closed-circle
}