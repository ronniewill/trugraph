import debug from 'debug'
import * as d3 from 'd3'

export function showTooltip(containerID, traceData, coords) {
    const log = debug('TruGraph:showTooltip:log')
    const error = debug('TruGraph:showTooltip:error')
    let tooltip = d3.select(`#${containerID}-tooltip`)
    tooltip.text(traceData.expression)
      .style('top', `${coords[1] + 50}px`)
      .style('left', `${coords[0]}px`)
      .style('display', 'block')
      .style('background', '#FFFFFF')
      .style('border', `2px solid ${traceData.colour}`)
      .style('transform', `translateX(-50%)`)
    log(traceData)
}