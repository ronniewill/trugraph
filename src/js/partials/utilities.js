import debug from 'debug'

const log = debug('TruGraph:utilities:log')
const error = debug('TruGraph:utilities:error')

export async function checkLayout (newLayout, compareLayout) {
  try {
    log('Checking checkLayout.')
    let defaultProps = Object.keys(compareLayout)
    let newProps = Object.keys(newLayout)
    let missingProps = defaultProps
    for (let i = 0, j; i < newProps.length; i++) {
      j = missingProps.indexOf(newProps[i])
      if (j > -1) missingProps.splice(j,1)
    }
    if (missingProps.length === 0) return newLayout
    for (let i = 0; i < missingProps.length; i++) {
      log(missingProps[i])
      newLayout[missingProps[i]] = compareLayout[missingProps[i]]
    }
    return newLayout
  } catch (err) {
    error(err)
  }
}

export async function checkObject (newObject, compareObject) {
  try {
    log('Checking object')
    let defaultProps = Object.keys(compareObject)
    let newProps = Object.keys(newObject)
    let missingProps = defaultProps
    for (let i = 0, j; i < newProps.length; i++) {
      j = missingProps.indexOf(newProps[i])
      if (j > -1) missingProps.splice(j,1)
    }
    for (let i = 0; i < missingProps.length; i++) {
      log(missingProps[i])
      newLayout[missingProps[i]] = compareLayout[missingProps[i]]
    }
    for (let k = 0; k < defaultProps.length; k++) {
      if (compareObject[defaultProps[k]] instanceof Object && compareObject[defaultProps[k]] !== null) {
        newObject[defaultProps[k]] = await checkObject(newObject[defaultProps[k]], compareObject[defaultProps[k]])
      }
    }
    return newObject
  } catch(err) {

  }
}