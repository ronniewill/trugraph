export let defaultOptions = {
  debug: true,
  responsive: true,
  menu: {
    zoom: true,
    trace: true
  }
}