import * as d3 from 'd3'
import debug from 'debug'
import { checkLayout } from './utilities.js'
import { defaultLayout } from './defaultLayout.js'
import { arrowhead } from './endpoints.js'
import { squareDims } from './dimensions.js'
import { showTooltip } from './tooltip.js'
// import * as MathExpression from 'math-expression-evaluator'
import { evaluate, create as mathCreate, all, sin } from 'mathjs'

const math = mathCreate(all, {})

export async function create (updateContainerID, containerID, layout) {
  const log = debug('TruGraph:create:log')
  const error = debug('TruGraph:create:error')
  try {
    let checkedLayout = await checkLayout(layout, defaultLayout)
    let container = d3.select(updateContainerID)
    let titleContainer
    if (layout.graphTitle.trim() !== '' && typeof layout.graphTitle !== 'undefined') {
      titleContainer = container.append('label')
        .attr('id', `${containerID}-graphTitle`)
        .style('display', 'block')
        .style('text-align', 'center')
        .text(layout.graphTitle)
    }
    let svgContainer = container.append('svg')
      .attr('id', `${containerID}-svg`)
    let axisGroup = svgContainer.append('g')
      .attr('id', `${containerID}-axisGroup`)
    let gridGroup = axisGroup.append('g')
      .attr('id', `${containerID}-gridGroup`)
    let horizontalGridGroup = gridGroup.append('g')
      .attr('id', `${containerID}-horizontalGridGroup`)
    let verticalGridGroup = gridGroup.append('g')
      .attr('id', `${containerID}-verticalGridGroup`)
    let traceGroup = svgContainer.append('g')
      .attr('id', `${containerID}-traceGroup`)
    let borderGroup = svgContainer.append('g')
      .attr('id', `${containerID}-borderGroup`)
    let xAxisGroup = axisGroup.append('g')
      .attr('id', `${containerID}-xAxisGroup`)
    let yAxisGroup = axisGroup.append('g')
      .attr('id', `${containerID}-yAxisGroup`)
    let containerSVG = document.querySelector(`#${containerID}-svg`)
    containerSVG.setAttribute('style', 'width:100%;height:100%;')
    let tooltip = container.append('div')
      .attr('id', `${containerID}-tooltip`)
      .attr('style', 'display:none;position:absolute;')
    tooltip.style('border', '1px solid black')
    tooltip.style('border-radius', '5px')
    tooltip.style('padding', '10px')
    let dim
    dim = containerSVG.getBoundingClientRect()
    let bodyHeight
    let bodyWidth
    let bufferX
    let bufferY
    if (checkedLayout.square === false) {
      bodyHeight = dim.height - 50
      bodyWidth = dim.width - 50
      bufferX = 50
      bufferY = 50
    } else {
      let dims = await squareDims (dim.width, dim.height)
      bodyHeight = dims.height - 50
      bodyWidth = dims.width - 50
      bufferX = dims.xDiff + 50
      bufferY = dims.yDiff + 50
    }
    let xMin = (typeof checkedLayout.visibleDomain[0] === 'undefined' ? checkedLayout.domain[0] : checkedLayout.visibleDomain[0])
    let xMax = (typeof checkedLayout.visibleDomain[1] === 'undefined' ? checkedLayout.domain[1] : checkedLayout.visibleDomain[1])
    let yMin = (typeof checkedLayout.visibleRange[0] === 'undefined' ? checkedLayout.range[0] : checkedLayout.visibleRange[0])
    let yMax = (typeof checkedLayout.visibleRange[1] === 'undefined' ? checkedLayout.range[1] : checkedLayout.visibleRange[1])
    let yScale = d3.scaleLinear()
      .range([bodyHeight, 0])
      .domain([yMin, yMax])
    let xScale = d3.scaleLinear()
      .range([0,bodyWidth])
      .domain([xMin, xMax])
    if (checkedLayout.border === true) {
      let leftBorder = borderGroup.append('line')
        .attr('id', 'leftBorder')
        .attr('x1', `${xScale(xMin) + (bufferX / 2)}`)
        .attr('y1', `${yScale(yMax) + (bufferY / 2)}`)
        .attr('x2', `${xScale(xMin) + (bufferX / 2)}`)
        .attr('y2', `${yScale(yMin) + (bufferY / 2)}`)
        .attr('style', `stroke:${checkedLayout.borderColour};stroke-width:${checkedLayout.borderWidth}`)
      let rightBorder = borderGroup.append('line')
        .attr('id', 'rightBorder')
        .attr('x1', `${xScale(xMax) + (bufferX / 2)}`)
        .attr('y1', `${yScale(yMax) + (bufferY / 2)}`)
        .attr('x2', `${xScale(xMax) + (bufferX / 2)}`)
        .attr('y2', `${yScale(yMin) + (bufferY / 2)}`)
        .attr('style', `stroke:${checkedLayout.borderColour};stroke-width:${checkedLayout.borderWidth}`)
      let topBorder = borderGroup.append('line')
        .attr('id', 'topBorder')
        .attr('x1', `${xScale(xMin) + (bufferX / 2)}`)
        .attr('y1', `${yScale(yMax) + (bufferY / 2)}`)
        .attr('x2', `${xScale(xMax) + (bufferX / 2)}`)
        .attr('y2', `${yScale(yMax) + (bufferY / 2)}`)
        .attr('style', `stroke:${checkedLayout.borderColour};stroke-width:${checkedLayout.borderWidth}`)
      let bottomBorder = borderGroup.append('line')
        .attr('id', 'bottomBorder')
        .attr('x1', `${xScale(xMin) + (bufferX / 2)}`)
        .attr('y1', `${yScale(yMin) + (bufferY / 2)}`)
        .attr('x2', `${xScale(xMax) + (bufferX / 2)}`)
        .attr('y2', `${yScale(yMin) + (bufferY / 2)}`)
        .attr('style', `stroke:${checkedLayout.borderColour};stroke-width:${checkedLayout.borderWidth}`)
    }
    if (checkedLayout.x.gridLines === true) {
      let xStart = checkedLayout.x.gridStart
      if (xStart < xMin) {
        while (xStart < xMin) xStart = xStart + checkedLayout.x.gridSpace
      } else if (xStart > xMin) {
        while (xStart > xMin) xStart = xStart - checkedLayout.x.gridSpace
        xStart = xStart + checkedLayout.x.gridSpace
      }
      let gridCountX = xStart
      if (gridCountX === xMin) gridCountX = xStart + checkedLayout.x.gridSpace
      while(gridCountX < xMax) {
        verticalGridGroup.append('line')
          .attr('x1', `${xScale(gridCountX) + (bufferX / 2)}`)
          .attr('y1', `${yScale(yMax) + (bufferY / 2)}`)
          .attr('x2', `${xScale(gridCountX) + (bufferX / 2)}`)
          .attr('y2', `${yScale(yMin) + (bufferY / 2)}`)
          .attr('stroke-dasharray', `1,1`)
          .attr('style', `stroke:#CCCCCC;stroke-width:1`)
        gridCountX = gridCountX + checkedLayout.x.gridSpace
      }
    }
    if (checkedLayout.y.gridLines === true) {
      let yStart = checkedLayout.y.gridStart
      if (yStart < yMin) {
        while (yStart < yMin) yStart = yStart + checkedLayout.y.gridSpace
      } else if (yStart > yMin) {
        while (yStart > yMin) yStart = yStart - checkedLayout.y.gridSpace
        yStart = yStart + checkedLayout.y.gridSpace
      }
      let gridCountY = yStart
      if (gridCountY === yMin) gridCountY = yStart + checkedLayout.y.gridSpace
      while(gridCountY < yMax) {
        horizontalGridGroup.append('line')
          .attr('x1', `${xScale(xMin) + (bufferX / 2)}`)
          .attr('y1', `${yScale(gridCountY) + (bufferY / 2)}`)
          .attr('x2', `${xScale(xMax) + (bufferX / 2)}`)
          .attr('y2', `${yScale(gridCountY) + (bufferY / 2)}`)
          .attr('stroke-dasharray', `1,1`)
          .attr('style', `stroke:#CCCCCC;stroke-width:1`)
        gridCountY = gridCountY + checkedLayout.y.gridSpace
      }
    }
    let xVals = []
    let xValStart = checkedLayout.x.tickStart
    if (xValStart < xMin) {
      while (xValStart < xMin) xValStart = xValStart + checkedLayout.x.tickSpace
    } else if (xValStart > xMin) {
      while (xValStart > xMin) xValStart = xValStart - checkedLayout.x.tickSpace
      xValStart = xValStart + checkedLayout.x.tickSpace
    }
    while (xValStart < xMax) {
      if (xValStart > xMin && xValStart < xMax) xVals.push(xValStart)
      xValStart = xValStart + checkedLayout.x.tickSpace
    }
    log('xVals: ', xVals)
    let axisX = d3.axisBottom(xScale)
      .tickFormat(d3.format('.2'))
      .tickValues(xVals)
    xAxisGroup.attr('transform', `translate(${0 + (bufferX / 2)}, ${yScale(0) + (bufferY / 2)})`)
      .call(axisX)
    // let getTitle = document.querySelector(`#${containerID}-graphTitle`)
    //let titleHeight = getTitle.getBoundingClientRect()
    // titleContainer.attr('transform', `translate(${0 + (bufferX / 2)}, ${titleHeight.height})`)
    //   .attr('alignment-baseline', 'top')
    let yVals = []
    let yValStart = checkedLayout.y.tickStart
    if (yValStart < yMin) {
      while (yValStart < yMin) yValStart = yValStart + checkedLayout.y.tickSpace
    } else if (yValStart > yMin) {
      while (yValStart > yMin) yValStart = yValStart - checkedLayout.y.tickSpace
      yValStart = yValStart + checkedLayout.y.tickSpace
    }
    while (yValStart < yMax) {
      if (yValStart > yMin && yValStart < yMax) yVals.push(yValStart)
      yValStart = yValStart + checkedLayout.y.tickSpace
    } 
    let axisY = d3.axisLeft(yScale)
      .tickFormat(d3.format('.2'))
      .tickValues(yVals)
    yAxisGroup.attr('transform', `translate(${xScale(0) + (bufferX / 2)}, ${0 + (bufferY / 2)})`)
      .call(axisY)
    xAxisGroup.append('polygon')
      .attr('points', `${xScale(xMax)},${yScale(yMax)} ${xScale(xMax)-10},${yScale(yMax)+5} ${xScale(xMax)-10},${yScale(yMax)-5}`)
      .attr('style', `fill:#000000`)
    xAxisGroup.append('polygon')
      .attr('points', `${xScale(xMin)},${yScale(yMax)} ${xScale(xMin)+10},${yScale(yMax)+5} ${xScale(xMin)+10},${yScale(yMax)-5}`)
      .attr('style', `fill:#000000`)
    yAxisGroup.append('polygon')
      .attr('points', `0,${yScale(yMax)} ${0-5},${yScale(yMax)+10} ${0+5},${yScale(yMax)+10}`)
      .attr('style', `fill:#000000`)
    yAxisGroup.append('polygon')
      .attr('points', `0,${yScale(yMin)} ${0-5},${yScale(yMin)-10} ${0+5},${yScale(yMin)-10}`)
      .attr('style', `fill:#000000`)
    return [xScale, yScale, checkedLayout, bufferX, bufferY]
  } catch (err) {
    error('create failed unexpectedly.', err)
  }
}

export async function createTrace (traceObject, layoutObject) {
  const log = debug('TruGraph:createTrace:log')
  const error = debug('TruGraph:createTrace:error')
  try {
    log(`Creating Trace: ${traceObject.expression}`)
    // let expression = MathExpression.fromText(traceObject.expression)
    let expression = traceObject.expression
    let minVisRange = layoutObject.visibleRange[0]
    let maxVisRange = layoutObject.visibleRange[1]
    let minVisDomain = layoutObject.visibleDomain[0]
    let maxVisDomain = layoutObject.visibleDomain[1]
    let dVar = layoutObject.dependentVar
    let iVar = layoutObject.independentVar
    
    let lineXArray = []
    let lineYArray = []
    let space = (maxVisDomain - minVisDomain)/10000
    let newArray = false
    let tempXArray = []
    let tempYArray = []
    for (let i = minVisDomain; i <= maxVisDomain; i += space) {
      let tempObj = {}
      let x = i
      tempObj[iVar] = x
      let y = evaluate(expression, tempObj)
      if (y >= minVisRange && y <= maxVisRange) {
        if (newArray === true) {
          lineXArray.push(tempXArray)
          lineYArray.push(tempYArray)

          tempXArray = []
          tempYArray = []
          newArray = false
        }
        tempXArray.push(x)
        tempYArray.push(y)
      } else {
        newArray = true
      }
    }
    lineXArray.push(tempXArray)
    lineYArray.push(tempYArray)
    return [lineXArray, lineYArray]
  } catch (err) {
    error ('createTrace failed unexpectedly.', err)
  }
}

export async function buildTrace (traceData, layout, trace, iterate, containerID, xScale, yScale, bufferX, bufferY) {
  const log = debug('TruGraph:buildTrace:log')
  const error = debug('TruGraph:buildTrace:error')
  try {
    log('Building a new trace.', trace)
    // let expression = traceData.expression
    // let deriveExpression = expression.derivative(layout.independentVar)
    let xTraces = trace[0]
    let yTraces = trace[1]
    let data = []
    for (let j = 0; j < xTraces.length; j++) {
      let tempArray = []
      for (let k = 0; k < xTraces[j].length; k++) {
        let tempObj = {}
        tempObj.xVal = xTraces[j][k]
        tempObj.yVal = yTraces[j][k]
        tempArray.push(tempObj)
      }
      data.push(tempArray)
    }
    for (let x = (data.length - 1); x >= 0; x--) {
      if (data[x].length === 0) data.splice(x,1)
    }
    let traceGroup = d3.select(`#${containerID}-traceGroup`)
    let thistrace = traceGroup.append('g')
      .attr('id', `${containerID}-trace${iterate}`)
    let newLine = d3.line()
      .x(d => xScale(d.xVal) + (bufferX / 2))
      .y(d => yScale(d.yVal) + (bufferY / 2))
      .defined(d => d.yVal)
    for (let m = 0; m < data.length; m++) {
      let xMin = d3.min(data[m],
        d => d.xVal)
      let xMax = d3.max(data[m],
        d => d.xVal)
      let yMin = data[m][0].yVal
      let yMax = data[m][(data[m].length - 1)].yVal
      let minObj = {}
      let maxObj = {}
      minObj[layout.independentVar] = xMin
      minObj[layout.dependentVar] = yMin
      maxObj[layout.independentVar] = xMax
      maxObj[layout.dependentVar] = yMax
      log('datam', data[m])
      let yMinSlope = -(yScale(data[m][1].yVal) - yScale(data[m][0].yVal)) / (xScale(data[m][1].xVal) - xScale(data[m][0].xVal))
      let yMaxSlope = -(yScale(data[m][data[m].length - 1].yVal) - yScale(data[m][data[m].length - 2].yVal)) / (xScale(data[m][data[m].length - 1].xVal) - xScale(data[m][data[m].length - 2].xVal))
      log(yMinSlope)
      log(yMaxSlope)
      let minAngle = (Math.atan(yMinSlope) * 180 ) / Math.PI
      let maxAngle = (Math.atan(yMaxSlope) * 180 ) / Math.PI
      log(minAngle)
      log(maxAngle)
      thistrace.append('path')
        .datum(data[m])
        .attr('d', newLine)
        .attr('style', `fill:none;stroke:${traceData.colour};stroke-width:${traceData.thickness}`)
        .on('mouseenter', d => {
          showTooltip(containerID, traceData, [d3.event.clientX, d3.event.clientY])
        })
        .on('mousemove', d => {
          showTooltip(containerID, traceData, [d3.event.clientX, d3.event.clientY])
        })
        .on('mouseleave', d => {
          d3.select(`#${containerID}-tooltip`)
            .style('display', 'none')
        })
      if (traceData.endpoints === true) {
        if (traceData.endpointLeft === 'arrow') {
          thistrace.append('polygon')
            .attr('points', `${arrowhead[0][0]},${arrowhead[0][1]} ${arrowhead[1][0]},${arrowhead[1][1]} ${arrowhead[2][0]},${arrowhead[2][1]}`)
            .attr('style', `fill:${traceData.colour};stroke:${traceData.colour};stroke-width:${traceData.thickness - 1};`)
            .attr('transform', `translate(${xScale(xMin) + (bufferX / 2)},${yScale(yMin) + (bufferY / 2)}) rotate(${270 - minAngle} 0 0)`)
        }
        if (traceData.endpointRight === 'arrow') {
          thistrace.append('polygon')
            .attr('points', `${arrowhead[0][0]},${arrowhead[0][1]} ${arrowhead[1][0]},${arrowhead[1][1]} ${arrowhead[2][0]},${arrowhead[2][1]}`)
            .attr('style', `fill:${traceData.colour};stroke:${traceData.colour};stroke-width:${traceData.thickness - 1};`)
            .attr('transform', `translate(${xScale(xMax) + (bufferX / 2)},${yScale(yMax) + (bufferY / 2)}) rotate(${90 - maxAngle} 0 0) `)
        }
      }
    }
  } catch (err) {
    error('buildTrace failed unexpectedly.', err)
  }
}