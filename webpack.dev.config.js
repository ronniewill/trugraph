const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const config = require('./webpack.config');
const path = require('path')

module.exports = merge(config, {
  mode: 'development',
  devtool: 'inline-source-map',
  entry: {
    testing: path.join(__dirname, 'demo/js/app.js')
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Development Testing',
      template: path.join(__dirname, 'demo/html/index.html'),
      filename: 'index.html',
      inject: true
    }),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 3000,
      proxy: 'http://localhost:8080/'
    })
  ],
  devServer: {
    port: 8080,
    hot: true,
  },
  stats: {
    colors: true,
    hash: false,
    version: false,
    timings: false,
    assets: false,
    chunks: false,
    modules: false,
    reasons: false,
    children: false,
    source: false,
    errors: true,
    errorDetails: false,
    warnings: false,
    publicPath: false
  }
});